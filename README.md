## w3m-img

You've probably used feh or imv to view your images in the past. But even though
they're minimal, they're still too heavy! What if there were a way to view images
that didn't even need an extra window at all?

There is: just use `w3m` to view images directly in your terminal!

Needs: `w3m` with image support (may be included or need a package like `w3m-img`
depending on your distro), a terminal that supports w3m images (`urxvt` is the
standard).

## Usage

`w3m-img.sh foo.jpg bar.jpg baz.png`

Large images will be stacked; small images will be placed next to each other.
